<?php
/**
 * @package  AlecadddPlugin
 */
/*
Plugin Name: MooseRaffle Plugin
Plugin URI: http://dev.charitymoose.com
Description: This plugin is created for working with charity moose website, to get checkout event from website and create tickets against each $1. After annoucing winner ticket , it will be displayed to Moose Website.
Version: 1.0.0
Author: Ameer Harram
Author URI: http://dev.charitymoose.com
License: GPLv2 or latter
Text Domain: moose-raffle-plugin
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2005-2015 Automattic, Inc.
*/

// If this file is called firectly, abort!!!
defined( 'ABSPATH' ) or die( 'Hey, what are you doing here? You silly human!' );

// Require once the Composer Autoload
if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

/**
 * The code that runs during plugin activation
 */
function activate_alecaddd_plugin() {
	Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_alecaddd_plugin' );

/**
 * The code that runs during plugin deactivation
 */
function deactivate_alecaddd_plugin() {
	Inc\Base\Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, 'deactivate_alecaddd_plugin' );

/**
 * Initialize all the core classes of the plugin
 */
if ( class_exists( 'Inc\\Init' ) ) {
	Inc\Init::register_services();
}

function checkConifOk()
{
	$flag = false;
	$id = get_option( 'active_contest_id' );
	$url = get_option( 'api_url' );
	$key = get_option( 'api_key' );
	
	if( $id == null || $id == '' || $id <= 0 )
	{
		return $flag;
	}

	if( $url == null || $url == ''  )
	{
		return $flag;
	}

	if( $key == null || $key == ''  )
	{
		return $flag;
	}

	$flag = true;
	
	return $flag;
}


use Inc\Base\HttpService;

// HttpService::defaultShortCodes();
add_action( 'plugins_loaded', 'start_service' );

function start_service(){
  
  	$current_user = wp_get_current_user(); 
	if( !checkConifOk() ){ return false; }
  	$http = new HttpService($current_user);
    $http->getLotteryDetails();
  

}

add_action('woocommerce_thankyou', 'checkout_success', 10, 1);
function checkout_success( $order_id ) {
	
	if( !checkConifOk() ){ return false; }
	$http = new HttpService($user_info);
	$data['order_number'] = $order_id.'';
	$http->creatLotteryTicket( $data );
	
}
