<?php 
/**
 * @package  AlecadddPlugin
 */
namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class AdminCallbacks extends BaseController
{
	public function adminDashboard()
	{
		return require_once( "$this->plugin_path/templates/admin.php" );
	}

	/*
	public function adminCpt()
	{
		return require_once( "$this->plugin_path/templates/cpt.php" );
	}

	public function adminTaxonomy()
	{
		return require_once( "$this->plugin_path/templates/taxonomy.php" );
	}

	public function adminWidget()
	{
		return require_once( "$this->plugin_path/templates/widget.php" );
	}
	*/

	public function alecadddOptionsGroup( $input )
	{
		return $input;
	}

	public function mooseRaffleAdminSection()
	{
		echo 'Integration settings with raffle module api.';
	}

	public function mooseRaffleActiveLotteryID()
	{
		$value = esc_attr( get_option( 'active_contest_id' ) );
		echo '<input type="number" min="0" class="regular-text" name="active_contest_id" value="' . $value . '" placeholder="Enter active lottery id">';
	}

	public function mooseRaffleDefaultShortCode()
	{
		$value = esc_attr( get_option( 'short_code_value' ) );
		echo '<input type="text"  class="regular-text" name="short_code_value" value="' . $value . '" placeholder="Enter default short code value..">';
	}

	public function mooseRaffleAPIUrl()
	{
		$value = esc_attr( get_option( 'api_url' ) );
		echo '<input type="text" class="regular-text" name="api_url" value="' . $value . '" placeholder="Enter api URL">';
	}


	public function mooseRaffleAPIKey()
	{
		$value = esc_attr( get_option( 'api_key' ) );
		echo ' <textarea  class="medium-text"  rows="5" cols="100" name="api_key" placeholder="Enter API key here">'. $value  .'</textarea>';
	}
}