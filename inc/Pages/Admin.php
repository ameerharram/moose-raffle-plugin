<?php 
/**
 * @package  AlecadddPlugin
 */
namespace Inc\Pages;

use Inc\Api\SettingsApi;
use Inc\Base\BaseController;
use Inc\Api\Callbacks\AdminCallbacks;

/**
* 
*/
class Admin extends BaseController
{
	public $settings;

	public $callbacks;

	public $pages = array();

	public $subpages = array();

	public function register() 
	{
		$this->settings = new SettingsApi();

		$this->callbacks = new AdminCallbacks();

		$this->setPages();

		$this->setSubpages(); // this function can create sub menu not in use right now....

		$this->setSettings();
		$this->setSections();
		$this->setFields();

		$this->settings->addPages( $this->pages )->withSubPage( 'Dashboard' )->addSubPages( $this->subpages )->register();
	}

	public function setPages() 
	{
		$this->pages = array(
			array(
				'page_title' => 'Moose Raffle', 
				'menu_title' => 'Moose Raffle', 
				'capability' => 'manage_options', 
				'menu_slug' => 'moose_raffle_plugin', 
				'callback' => array( $this->callbacks, 'adminDashboard' ), 
				'icon_url' => 'dashicons-megaphone', 
				'position' => 110
			)
		);
	}

	public function setSubpages()
	{
		$this->subpages = array(
			/*
			array(
				'parent_slug' => 'moose_raffle_plugin', 
				'page_title' => 'Custom Post Types', 
				'menu_title' => 'CPT', 
				'capability' => 'manage_options', 
				'menu_slug' => 'alecaddd_cpt', 
				'callback' => array( $this->callbacks, 'adminCpt' )
			),
			
			array(
				'parent_slug' => 'moose_raffle_plugin', 
				'page_title' => 'Custom Taxonomies', 
				'menu_title' => 'Taxonomies', 
				'capability' => 'manage_options', 
				'menu_slug' => 'alecaddd_taxonomies', 
				'callback' => array( $this->callbacks, 'adminTaxonomy' )
			),
			array(
				'parent_slug' => 'moose_raffle_plugin', 
				'page_title' => 'Custom Widgets', 
				'menu_title' => 'Widgets', 
				'capability' => 'manage_options', 
				'menu_slug' => 'alecaddd_widgets', 
				'callback' => array( $this->callbacks, 'adminWidget' )
			)
			*/
		);
	}

	public function setSettings()
	{
		$args = array(
			array(
				'option_group' => 'alecaddd_options_group',
				'option_name' => 'active_contest_id',
				'callback' => array( $this->callbacks, 'alecadddOptionsGroup' )
			),
			array(
				'option_group' => 'alecaddd_options_group',
				'option_name' => 'short_code_value',
				'callback' => array( $this->callbacks, 'alecadddOptionsGroup' )
			),
			array(
				'option_group' => 'alecaddd_options_group',
				'option_name' => 'api_url',
				'callback' => array( $this->callbacks, 'alecadddOptionsGroup' )
			),
			array(
				'option_group' => 'alecaddd_options_group',
				'option_name' => 'api_key'
			)
		);

		$this->settings->setSettings( $args );
	}

	public function setSections()
	{
		$args = array(
			array(
				'id' => 'alecaddd_admin_index',
				'title' => 'Settings',
				'callback' => array( $this->callbacks, 'mooseRaffleAdminSection' ),
				'page' => 'moose_raffle_plugin'
			)
		);

		$this->settings->setSections( $args );
	}

	public function setFields()
	{
		$args = array(
			array(
				'id' => 'active_contest_id',
				'title' => 'Active Lottery ( ID )',
				'callback' => array( $this->callbacks, 'mooseRaffleActiveLotteryID' ),
				'page' => 'moose_raffle_plugin',
				'section' => 'alecaddd_admin_index',
				'args' => array(
					'label_for' => 'active_contest_id',
					'class' => 'example-class'
				)
			),
			array(
				'id' => 'short_code_value',
				'title' => 'Short Code ( Default Value )',
				'callback' => array( $this->callbacks, 'mooseRaffleDefaultShortCode' ),
				'page' => 'moose_raffle_plugin',
				'section' => 'alecaddd_admin_index',
				'args' => array(
					'label_for' => 'short_code_value',
					'class' => 'example-class'
				)
			),
			array(
				'id' => 'api_url',
				'title' => 'API URL',
				'callback' => array( $this->callbacks, 'mooseRaffleAPIUrl' ),
				'page' => 'moose_raffle_plugin',
				'section' => 'alecaddd_admin_index',
				'args' => array(
					'label_for' => 'api_url',
					'class' => 'example-class'
				)
			),
			array(
				'id' => 'api_key',
				'title' => 'API KEY',
				'callback' => array( $this->callbacks, 'mooseRaffleAPIKey' ),
				'page' => 'moose_raffle_plugin',
				'section' => 'alecaddd_admin_index',
				'args' => array(
					'label_for' => 'api_key',
					'class' => 'example-class'
				)
			)
		);

		$this->settings->setFields( $args );
	}
}