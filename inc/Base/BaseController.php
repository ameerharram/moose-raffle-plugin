<?php 
/**
 * @package  AlecadddPlugin
 */
namespace Inc\Base;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class BaseController
{
	public $plugin_path;

	public $plugin_url;

	public $plugin;

	/*Routes*/
	public $CREATE_TICKETS;
	public $LOTTERY_DETAILS;
	public $LOTTERY_WINNER;

	public function __construct() {

		$this->plugin_path = plugin_dir_path( $this->dirname_r( __FILE__, 2 ) );
		$this->plugin_url = plugin_dir_url( $this->dirname_r( __FILE__, 2 ) );
		$this->plugin = plugin_basename( $this->dirname_r( __FILE__, 3 ) ) . '/alecaddd-plugin.php';


		$this->CREATE_TICKETS = 'create-tickets';
		$this->LOTTERY_DETAILS = 'get-lottery-detail';
		$this->LOTTERY_WINNER = 'get-winner/';


	}

	function dirname_r($path, $count=1){
	    if ($count > 1){
	       return dirname($this->dirname_r($path, --$count));
	    }else{
	       return dirname($path);
	    }
	}
}