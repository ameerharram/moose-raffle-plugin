<?php 
/**
 * @package  AlecadddPlugin
 */
namespace Inc\Base;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class HttpService extends BaseController
{

   public $token;
   public $apiUrl;
   public $activeLottery;
   public $user;


   public function __construct( $current_user )
   {
   	 $this->token = 'Bearer ' . esc_attr( get_option( 'api_key' ) );
   	 $this->apiUrl =  esc_attr( get_option( 'api_url' ) );
   	 $this->activeLottery = esc_attr( get_option( 'active_contest_id' ) );
   	 $this->user = $current_user;

   }


	public function httpRequest($type,$params,$path,$exheaders = [])
    {
    	$url =  $this->apiUrl.'/'.$path;
    	$headers = [
					'Content-Type' => 'application/json',
    				'Authorization' => $this->token ,
			       ];
		
		$headers = array_merge($headers,$exheaders);	               
    	$client = new Client(['headers' => ['Accept' => 'application/json']]);
		
		$res = $client->request($type, $url,[

				'headers' => $headers,
		        'query' => $params
		    ]);
		return  json_decode($res->getBody(),true);
	}

	public function getLotteryDetails()
	{

		$path = 'get-lottery-detail';
		$params = [ 'wp_user_id' => $this->user->ID , 'lottery_id' => $this->activeLottery ];
	
		try {	

			$res = $this->httpRequest('POST',$params,$path);

			if($res['status'] == 'success')
			{
				$data = $res['responseData'];
				foreach ($data as $key => $value) {
					# code...
					add_shortcode( $key, function() use ( $value ){
						return $value ;
					});
					//wp_die($key);
				}
			}else
			{
				HttpService::defaultShortCodes();
			}
    
		}catch (GuzzleException $e) {
		     HttpService::defaultShortCodes();
		}

		

	}


	public function creatLotteryTicket( $params )
	{
		$path = 'create-tickets';
		try
		{
			$res = $this->httpRequest('POST',$params,$path);

			// var_dump( $res );
			// die;
		}
		catch (GuzzleException $e)
		{

			var_dump($e->getMessage());
			die;
		}

					

	}

	public static function defaultShortCodes()
	{
		
		add_shortcode( 'raffle_ends', function(){ return get_option( 'short_code_value' ); } );
		add_shortcode( 'participant_tickets', function(){ return get_option( 'short_code_value' ); } );
		add_shortcode( 'probablity', function(){ return get_option( 'short_code_value' ); } );
	}


}